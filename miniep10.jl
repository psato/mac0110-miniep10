function sum_submatrix_rightdown_stripes(v)
  len = size(v,1)
  soma = 0
  for i in 1:len
    soma += v[i, len]
  end
  for i in 1:len-1
    soma += v[len, i]
  end
  return soma
end

#  Dado uma matrix, encontrar as submatrizes quadradas com a maior soma dos
# elementos, onde o elemento m[1,1] estando obrigatoriamente na submatrix resultante.
# Retornando tbm a soma dos elementos da submatriz.
function biggest_sum_fixed_head(v)
  (size(v,1) == 1) && return ([v[1:1,1:1]], v[1,1])
  candidatearray = [v[1:1,1:1]]
  soma_acc = 0
  soma_max = v[1,1]
  for i in 1:size(v,1)
    minor_increment = sum_submatrix_rightdown_stripes(v[1:i,1:i])
    soma_acc += minor_increment
    if soma_acc > soma_max
      soma_max = soma_acc
      candidatearray = [v[1:i,1:i]]
    end
    if (soma_acc == soma_max) && (v[1:i,1:i] ∉ candidatearray)
      candidatearray = vcat(candidatearray, [v[1:i,1:i]])
    end
  end
  return (candidatearray, soma_max)
end

function max_sum_submatrix(v)
  returnarray = [v[1:1,1:1]]
  maxsum = v[1,1]
  len = size(v, 1)
  for linha in 1:len
    for coluna in 1:len
      lenmax = min(len-linha+1, len-coluna+1)
      candidates = biggest_sum_fixed_head(v[linha:linha+lenmax-1, coluna:coluna+lenmax-1])
      if candidates[2] > maxsum
        maxsum = candidates[2]
        returnarray = candidates[1]
      elseif (candidates[2] == maxsum) && (linha != 1 || coluna != 1)
        returnarray = vcat(returnarray, candidates[1])
      end
    end
  end
  return returnarray
end
